package com.ccc.mappers;

import com.ccc.model.ScheduleTraining;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
public interface ScheduleTrainingMapper {

    @Select("select * from schedule")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "trainingDay", column = "training_day"),
            @Result(property = "amount", column = "amount")
    })
    public List<ScheduleTraining> getAllSchedule();

    @Select("select * from schedule where id = #{id}")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "trainingDay", column = "training_day"),
            @Result(property = "amount", column = "amount")
    })
    public ScheduleTraining getScheduleById(int id);
}
