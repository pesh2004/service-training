package com.ccc.mappers;

import com.ccc.model.Employee;
import com.ccc.model.ScheduleTraining;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
public interface EmployeeMapper {
    @Select("select * from employee")
    @Results(value = {
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "code", column = "code"),
            @Result(property = "name", column = "name"),
            @Result(property = "department", column = "department"),
            @Result(property = "scheduleTraining", column = "status",
                    one = @One(select = "com.ccc.mappers.ScheduleTrainingMapper.getScheduleById"))
    })
    public List<Employee> getAllEmployee();

    @Select("select * from employee where code = #{param1}")
    @Results(value = {
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "code", column = "code"),
            @Result(property = "name", column = "name"),
            @Result(property = "department", column = "department"),
            @Result(property = "scheduleTraining", column = "status",
                    one = @One(select = "com.ccc.mappers.ScheduleTrainingMapper.getScheduleById"))
    })
    public Employee getEmployeeByCode(String codeSearch);


}
