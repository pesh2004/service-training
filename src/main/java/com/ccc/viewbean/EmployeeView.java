package com.ccc.viewbean;

import com.ccc.model.Employee;
import com.ccc.services.ScheduleTrainingService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
@ManagedBean (name = "employeeview")
@ViewScoped
public class EmployeeView {
    Employee employeeEntity;
    String codeSearch;

    //    @ManagedProperty("#{scheduletraining}")
//    private ScheduleTrainingService scheduleTrainingService;

    @PostConstruct
    public void init() {
    }

    public String lookupProfile(){
        System.out.println(codeSearch);
        return "";
    }

    public Employee getEmployeeEntity() {
        return employeeEntity;
    }

    public void setEmployeeEntity(Employee employeeEntity) {
        this.employeeEntity = employeeEntity;
    }

    public String getCodeSearch() {
        return codeSearch;
    }

    public void setCodeSearch(String codeSearch) {
        this.codeSearch = codeSearch;
    }
}
