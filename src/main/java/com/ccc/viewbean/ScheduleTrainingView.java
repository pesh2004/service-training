package com.ccc.viewbean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
@ManagedBean (name = "scheduletrainingview")
@ViewScoped
public class ScheduleTrainingView {

    int selectedSchedule;

    @PostConstruct
    public void init() {
    }

    public int getSelectedSchedule() {
        return selectedSchedule;
    }

    public void setSelectedSchedule(int selectedSchedule) {
        this.selectedSchedule = selectedSchedule;
    }
    
    public String checkAmount(){
        String result = "";
        return result;
    }

}
