package com.ccc.model;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
public class ScheduleTraining {
    private int id;
    private String trainingDay;
    private int amount;

    @Override
    public String toString() {
        return "ScheduleTraining{" +
                "id=" + id +
                ", trainingDay='" + trainingDay + '\'' +
                ", amount=" + amount +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTrainingDay() {
        return trainingDay;
    }

    public void setTrainingDay(String trainingDay) {
        this.trainingDay = trainingDay;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
