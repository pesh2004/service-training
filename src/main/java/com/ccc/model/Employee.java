package com.ccc.model;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
public class Employee {
    private int id;
    private String code;
    private String name;
    private String department;
    private ScheduleTraining scheduleTraining;

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", department='" + department + '\'' +
                ", scheduleTraining=" + scheduleTraining +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public ScheduleTraining getScheduleTraining() {
        return scheduleTraining;
    }

    public void setScheduleTraining(ScheduleTraining scheduleTraining) {
        this.scheduleTraining = scheduleTraining;
    }
}
