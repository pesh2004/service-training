package com.ccc.services;

import com.ccc.mappers.EmployeeMapper;
import com.ccc.model.Employee;
import com.ccc.model.ScheduleTraining;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.List;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
@ManagedBean (name = "employee")
@ApplicationScoped
public class EmployeeService implements EmployeeMapper {
    @Override
    public List<Employee> getAllEmployee() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            return employeeMapper.getAllEmployee();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Employee getEmployeeByCode(String codeSearch) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            return employeeMapper.getEmployeeByCode(codeSearch);
        } finally {
            sqlSession.close();
        }
    }
}
