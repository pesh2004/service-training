package com.ccc.services;

import com.ccc.mappers.ScheduleTrainingMapper;
import com.ccc.model.ScheduleTraining;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.List;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
@ManagedBean(name = "scheduletraining")
@ApplicationScoped
public class ScheduleTrainingService implements ScheduleTrainingMapper {
    @Override
    public List<ScheduleTraining> getAllSchedule() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            ScheduleTrainingMapper scheduleTrainingMapper = sqlSession.getMapper(ScheduleTrainingMapper.class);
            return scheduleTrainingMapper.getAllSchedule();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public ScheduleTraining getScheduleById(int id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            ScheduleTrainingMapper scheduleTrainingMapper = sqlSession.getMapper(ScheduleTrainingMapper.class);
            return scheduleTrainingMapper.getScheduleById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int updateAmountforSchedule(int amount, int id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            ScheduleTrainingMapper scheduleTrainingMapper = sqlSession.getMapper(ScheduleTrainingMapper.class);
            int effectrow = scheduleTrainingMapper.updateAmountforSchedule(amount,id);
            sqlSession.commit();
            return effectrow;
        } finally {
            sqlSession.close();
        }
    }
}
