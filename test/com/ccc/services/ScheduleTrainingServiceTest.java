package com.ccc.services;

import com.ccc.model.ScheduleTraining;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
public class ScheduleTrainingServiceTest {

    private ScheduleTrainingService scheduleTrainingService;
    @org.junit.Before
    public void setUp() throws Exception {
        scheduleTrainingService = new ScheduleTrainingService();

    }

    @org.junit.After
    public void tearDown() throws Exception {
        scheduleTrainingService = null;
    }

    @org.junit.Test
    public void testGetAllSchedule() throws Exception {
        List<ScheduleTraining> scheduleTrainings = scheduleTrainingService.getAllSchedule();
        for (ScheduleTraining scheduleTraining : scheduleTrainings){
            System.out.println(scheduleTraining);
        }
    }

    @Test
    public void testGetScheduleById() throws Exception {
        ScheduleTraining scheduleTraining = scheduleTrainingService.getScheduleById(1);
        System.out.println(scheduleTraining);
    }
}