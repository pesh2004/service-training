package com.ccc.services;

import com.ccc.model.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
public class EmployeeServiceTest {

    private EmployeeService employeeService;

    @Before
    public void setUp() throws Exception {
        employeeService = new EmployeeService();

    }

    @After
    public void tearDown() throws Exception {
        employeeService = null;

    }

    @Test
    public void testGetAllEmployee() throws Exception {
        List<Employee> employeeList = employeeService.getAllEmployee();
        for (Employee employee : employeeList){
            System.out.println(employee);
        }
    }

    @Test
    public void testGetEmployeeByCode() throws Exception {
        Employee employee = employeeService.getEmployeeByCode("801007");
        System.out.println(employee);

    }
}